<!DOCTYPE html>
<html lang="pt">
    <head>
        <title>Documentação da API</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <style>
        body {
            margin: 30px;
        }

        pre {
            width: 75%;
        }
    </style>
    <body>
        <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Iniciar o fluxo de autenticação <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/facebook/login/$token">http://fmf.joaoferreira.me/facebook/login/$token</small></a></h2>
            <!-- Descrição -->
            <p>Este serviço é composto por 2 pontos. <ul><li>Primeiro, a aplicação deve obter o token através do SDK do facebook.</li> <li>Segundo, deve enviar o token no URL deste pedido para obter os seus dados de registo o uuid de sessão a ser enviado pela aplicação ao servidor nos futuros serviços.</li></ul></p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o "$token" no url (em vez de ser no body).</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong> após login no Facebook</h4>
            <pre>
{
    "id": 2,
    "name": "João Ferreira",
    "email": "joao_ferreira1993@live.com.pt",
    "picture": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/15697590_1281451685209557_6192959740468503503_n.jpg?oh=e685efd83e3760953d39bec5b46f4e61&oe=59A7092D",
    "session": {
        "uuid": "77f95d25-97a4-4a9e-afe2-ef57db062cd1"
    }
}           </pre>
        </div>

        <!-- SERVIÇO # -->
        <hr>
        <div>
            <!-- Title -->
            <h2>Envio de Push <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/push/send/$token">http://fmf.joaoferreira.me/push/send/$token</small></a></h2>
            <!-- Descrição -->
            <p>Serviço de teste para envio de Push. Basta aceder a este link pelo browser. Devem obter um token previamente através do Google Cloud Messaging. <br>O servidor responde com o estado do envio. ver resposta.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o "$token" no url (em vez de ser no body).</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>object(stdClass)#200 (1) { ["error"]=> string(19) "InvalidRegistration" }</pre>    
        </div>
        
        <hr>
        <div>
            <!-- Title -->
            <h2>Obter amigos do utilizador <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/facebook/friends?uuid=$uuid">http://fmf.joaoferreira.me/facebook/friends?uuid=$uuid</small></a></h2>
            <!-- Descrição -->
            <p>Este é o método usado para obter os amigos do utilizador que se tenham autenticado através da aplicação.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o "$uuid" no url. Este uuid é obtido na autenticação do utilizador.</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>
[
    {
        "facebook_id": "10209254663664337",
        "name": "Pedro Simões",
        "picture": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/12814070_10206003065816423_8322282560228317704_n.jpg?oh=0943efd27213da947719b86b4c45d9d3&oe=59A6FC87"
    },
    {
        "facebook_id": "1539627132775019",
        "name": "Samuel Pereira",
        "picture": "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/1380302_555858371151905_1232944852_n.jpg?oh=f093574593fab232d61303e8298364b0&oe=59AEC857"
    }
]           </pre>    
        </div>

        <hr>
        <div>
            <!-- Title -->
            <h2>Criar grupo de amigos <span class="label label-info">POST</span> <small><a href="http://fmf.joaoferreira.me/groups/new?uuid=$uuid">http://fmf.joaoferreira.me/groups/new?uuid=$uuid</small></a></h2>
            <!-- Descrição -->
            <p>Este é o método usado para criar um novo grupo de amigos.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>JSON Body:
{
    "name":"xpto"
}           </pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
{   
    "id":11
}
O campo id refere-se ao id do grupo criado.

Erro:
    HTTP 400
    Message -> The group [xpto] already exists. </pre>    
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Lista de grupo de amigos <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/groups/list?uuid={uuid}">http://fmf.joaoferreira.me/groups/list?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Lista os vários grupos do utilizador mencionado no parâmetro.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o "uuid" no url.</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>
{
    [
        {
            "groupId": 2,
            "name": "Amazing group",
            "total_users": 3
        },
        ...
    ]
}           </pre>
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Sair do grupo de amigos <span class="label label-warning">DELETE</span> <small><a href="http://fmf.joaoferreira.me/groups/{groupId}/leave?uuid={uuid}">http://fmf.joaoferreira.me/groups/{groupId}/leave?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para o utilizador sair de um determinado grupo. É passado o id do grupo pelo Path Parameter "groupId"</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o id do grupo como path parameter {groupId} e enviar o {uuid} no url como query parameter. </pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200

    Message -> Success  

Erro:
    HTTP 400

    Message -> Inexistent group.</pre>
        </div>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Adicionar amigos a um grupo <span class="label label-info">POST</span> <small><a href="http://fmf.joaoferreira.me/groups/{groupId}/add?uuid={uuid}">http://fmf.joaoferreira.me/groups/{groupId}/add?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para o utilizador adicionar amigos a um determinado grupo. O servidor deve receber sempre um array, mesmo que só seja para adicionar um elemento. </p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o id do grupo como path parameter {groupId} e enviar o {uuid} no url como query parameter. 
{
    "elements": [ "1517566058285039", "1539627132775019" ]
}</pre>
            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200

    Message -> Success  

Erro:
    HTTP 400

    Message -> Inexistent group. (id do grupo nao é valido)
    Message -> Invalid element structure. (estrutura do array errada)
    Message -> Invalid element. (elemento do array invalido)
    Message -> Invalid user. (id do user a adicionar nao é valiado)
    Message -> Already a group member. (User já pertence ao grupo)</pre>
        </div>
            <div>
            <!-- Title -->
            <h2>Obter membros de um grupo <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/groups/{groupId}/elements?uuid={uuid}">http://fmf.joaoferreira.me/groups/{groupId}/elements?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este é o método usado para obter os elementos de um determinado grupo.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o id do grupo como path parameter {groupId} e enviar o {uuid} no url como query parameter. </pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>
[
    {
        "facebook_id": "10209254663664337",
        "name": "Pedro Simões",
        "picture": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/12814070_10206003065816423_8322282560228317704_n.jpg?oh=0943efd27213da947719b86b4c45d9d3&oe=59A6FC87"
    },
    {
        "facebook_id": "1539627132775019",
        "name": "Samuel Pereira",
        "picture": "https://scontent.xx.fbcdn.net/v/t1.0-1/s200x200/1380302_555858371151905_1232944852_n.jpg?oh=f093574593fab232d61303e8298364b0&oe=59AEC857"
    }
]           </pre>    
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Registar o token das push notifications <span class="label label-warning">POST</span> <small><a href="http://fmf.joaoferreira.me/push/register?uuid={uuid}">http://fmf.joaoferreira.me/push/register?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para a aplicação cliente registar o token das push. Este endpoint deve ser invocado quando a aplicação obtém um novo token das push.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>{
    "pushToken": "abscaopadsodkpaosdmaposodjaodj21093u10rji130r8910j1p2"
}</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200

    Message -> Success  

Erro:
    HTTP 400

    Message -> Bad parameters. You should provide a push token.</pre>
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Actualizar a posição <span class="label label-warning">POST</span> <small><a href="http://fmf.joaoferreira.me/position/set?uuid={uuid}">http://fmf.joaoferreira.me/position/set?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para a definir a posição actual de um utilizador, quando esta é requisitada. O request_id é obtido na push</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
<pre>Se tiver a partilha de localização activa
{
    "success": true,
    "request_id" : 2,
    "position" : {
        "latitude": -42.2323,
        "longitude": 8.2323
    }
}

Se não tiver a partilha de localização activa
{
    "success": false,
    "request_id" : 2
}</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200

    Message -> Success  

Erro:
    HTTP 400

    Message -> Invalid request strucutre. (Estrutura de pedido inválida)
    Message -> Invalid request. (request_id inválido)
    Message -> Invalid access. (User não tem acesso ao request_id)
    Message -> Position already defined. (User já definiu a sua localização)</pre>
        </div>


        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Detalhes das Push <span class="label label-default">Push</span></h2>
            <!-- Descrição -->
            <p>Detalhes das estruturas das push enviadas pelo servidor. Acções disponíveis na variavel action_key:
            <ul>
                <li> 0 - Não fazer nada, apenas mostrar a mensagem e o título na notification bar.</li>
                <li> 1 - Enviar a posição do user ao servidor. Ver pedido "Actualizar a posição". Este tipo de push não deve ser mostrada na notification bar.</li>
                <li> 2 - Mostrar ao utilizador a posição de um amigo. É enviada uma mensagem e um título para mostrar na notification bar.</li>
            </ul></p>
            
            <!-- Request -->
            <h4>Mostrar mensagem e título na notification bar</h4>
<pre>{
    "action_key": 0 
}</pre>
            <!-- Request -->
            <h4>Obter a posição de um user</h4>
<pre>{
    "request_id" : 1,
    "action_key": 1 
}</pre>
            <!-- Request -->
            <h4>Partilha de posição de um user</h4>
<pre>{
    "facebook_id" : 2,
    "name": "Victor",
    "email": "victor@email.com",
    "picture" : "scontent.xx.fbcdn.net/v/t1.0-1/p200x200/15697590_1281451685209557_6192959740468503503_n.jpg?oh=e685efd83e3760953d39bec5b46f4e61&oe=59A7092D"
    "action_key": 2, 
    "position" : {
        "latitude": -42.2323,
        "longitude": 8.2323
    }
}</pre>
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Pedir ajuda aos elementos do grupo <span class="label label-warning">POST</span> <small><a href="http://fmf.joaoferreira.me/groups/{groupId}/help?uuid={uuid}">http://fmf.joaoferreira.me/groups/{groupId}/help?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para pedir ajuda aos membros de um determinado grupo enviando as suas cordenadas no pedido de ajuda.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o id do grupo como path parameter {groupId} e enviar o {uuid} no url como query parameter. 
{
    "position": {
        "latitude": -42.2323,
        "longitude": 8.2323
    }
}</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200

    Message -> Success  

Erro:
    HTTP 400

    Message -> The user doesnt exist.
    Message -> The group dont exists or you dont belong there.
    Message -> Bad structure. You should provide a valid structure.
    Message -> Group dont exits or its empty.
    Message -> Something went wrong sending the push notification.</pre>
        </div>

        <hr>
         <!-- SERVIÇO # -->
        <div>
            <!-- Title -->
            <h2>Pedido de localização de um utilizador <span class="label label-success">GET</span> <small><a href="http://fmf.joaoferreira.me/positions/{facebookId}?uuid={uuid}">http://fmf.joaoferreira.me/positions/{facebookId}?uuid={uuid}</small></a></h2>
            <!-- Descrição -->
            <p>Este endpoind é usado para pedir a licalização de um amigo do utilizador.</p>
            
            <!-- Request -->
            <h4>Enviado pelo <strong>Cliente</strong></h4>
            <pre>Enviar o id do facebook do user a localizar.</pre>

            <!-- Response -->
            <h4>Enviado pelo <strong>Servidor</strong></h4>
            <pre>Sucesso:
    HTTP 200
{
    "success" : true,
    "message" : "Success"
    "position": {
        "latitude": -42.2323,
        "longitude": 8.2323
    }
}

Erro:
    HTTP 400
{
    "success" : false,
    "message" : "Could not execute the position request. The user [123456] does not have a valid push token."
}

    Message -> Bad facebook_id request. You and [123456] are not friends.
    Message -> Something went wrong sending the push notification.</pre>
        </div>
    </body>
</html>