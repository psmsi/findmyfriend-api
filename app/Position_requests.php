<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position_requests extends Model
{
    protected $table = 'position_requests';
    
    public $timestamps = false;

    /**
     * Get the user whose his position was requested.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get user's position request applicant.
     */
    public function positionRequestApplicant()
    {
        return $this->hasMany('App\Position_request_applicant');
    }
}
