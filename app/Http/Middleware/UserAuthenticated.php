<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Sessions;

class UserAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !Input::has('uuid')) {
            return Response::json([ 'code' =>  401, 'message' =>  'No uuid provided.' ], 401);
        }
        
        if(! (Sessions::with('user')->where('uuid', Input::get('uuid'))->count() > 0)) {
            return Response::json([ 'code' =>  401, 'message' =>  'Invalid uuid.' ], 401);
        }

        return $next($request);
    }
}
