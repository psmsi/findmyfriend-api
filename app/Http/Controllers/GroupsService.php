<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Groups;
use App\Groups_elements;
use App\User;
use App\Sessions;


class GroupsService extends Controller
{
	public function addGroup(Request $request){
		$body = json_decode($request->getContent(), TRUE);
		$groupName = isset($body['name']) ? $body['name'] : NULL;
		$existentGroups = Groups::where('name', $groupName)->exists();
		if(!$existentGroups){
			$newGroup = new Groups();
			$newGroup->name = $groupName;
			$newGroup->total_users = 0;

			$session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
			$user = $session->user;
			$newGroupRelation = new Groups_elements();

			DB::transaction(function () use($newGroup, $user, $newGroupRelation) {
	            $newGroup->save();
	            $newGroupRelation->group()->associate($newGroup);
	            $newGroupRelation->user()->associate($user);
	            $newGroupRelation->save();
	            $newGroup->total_users = $newGroup->total_users + 1;
	            $newGroup->save();
        	});

			return ['id' => $newGroup->id]; 
		}
		return response('The group [' . $groupName . '] already exists.', 400);
	}
	
	public function listGroups(Request $request){
		$session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
		$groupsList = Groups_elements::with('group')->where('user_id', $session->user->id)->get();
		
		$groups = [];

		for ($i=0; $i < sizeof($groupsList); $i++) { 
			$groups[] = $groupsList[$i]->group;
		}
		return $groups;
	}

	public function leaveGroup($groupId){
		$session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
		$user = $session->user;
		$groupElements = Groups_elements::with('group')->where('user_id', $user->id)->where('group_id', $groupId)->first();
		if($groupElements == null){
			return response('Inexistent group.', 400);
		}
		
		$group = $groupElements->group;
		DB::transaction(function () use($groupElements, $group, $user, $groupId) {
			//Workarround relative to this instruction -> $groupElements->delete(); -> Error: "Undefined column: 7 ERROR: column "id" does not exist"
			DB::table('groups_elements')->where('user_id', $user->id)->where('group_id', $groupId)->delete();
			$group->total_users--;
			$group->total_users == 0 ? $group->delete() : $group->save();
		});
		return response('Success');
	}

	public function addElements($groupId, Request $request) 
	{
		$group = Groups::find($groupId); 

		if ($group == NULL) {
			return response('Inexistent group.', 400);
		}

		$body = json_decode($request->getContent(), TRUE);

		if(!isset($body['elements']) || !is_array($body['elements'])) {
			return response('Invalid element structure.', 400);
		} 

		$elements = $body['elements'];

		$usersIds = [];
		foreach ($elements as $element) {
			$user = User::with('groups')->where('facebook_id', $element)->first();

			if($user == NULL) {
				return response('Invalid user.', 400);
			}

			if ($user->groups()->where('group_id', $group->id)->count() > 0){
				return response('Already a group member.', 400);
			}

			$usersIds[] = $user->id;
		}

		DB::transaction(function () use($usersIds, $group) {
			foreach ($usersIds as $userId) {
				
	            Groups_elements::create([
					'user_id' => $userId,
					'group_id' => $group->id
				]);

				$group->total_users++;
				$group->save();
			}	
		});

		return response('Success');
	}

	public function getElements($groupId) 
	{
		$groupElements = Groups_elements::with('user')->where('group_id', $groupId)->get();
		$parsedElements = [];

		if ($groupElements != NULL) {
			foreach ($groupElements as $element) {
				$parsedElements[] = $element->user;
			}
		}

		return $parsedElements;
	}
}