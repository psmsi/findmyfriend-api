<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Sessions;

class PushService extends Controller
{
    
    public function sendPush($pushToken = NULL) {

    	$push = new PushNotification('fcm');

    	$push->setMessage([
            'notification' => [
                    'title'=>'This is the title',
                    'body'=>'This is the message',
                    'sound' => 'default'
                    ],
            'data' => [
                    'extraPayLoad1' => 'value1',
                    'extraPayLoad2' => 'value2'
                    ]
            ])
            ->setDevicesToken([$pushToken])
            ->send();
            
            var_dump($push->getFeedback()->results[0]);
    }

    public function registerToken(Request $request){
        $body = json_decode($request->getContent(), TRUE);
        $pushToken = NULL; 
        if(isset($body['pushToken'])){
            $pushToken = $body['pushToken'];
        }else{
            return response('Bad parameters. You should provide a push token.', 400);
        }

        $session = Sessions::where('uuid', Input::get('uuid'))->first();

        DB::transaction(function () use($session, $pushToken) {
            $session->push_token = $pushToken;
            $session->save();
        });  

        return response("Success", 200);
    }
}
