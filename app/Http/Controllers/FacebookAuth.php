<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SammyK;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

//Domain
use App\User;
use App\Sessions;

class FacebookAuth extends Controller
{
    public function getFacebookLoginURL($facebookToken, SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {

        try {
            $oauth_client = $fb->getOAuth2Client();
            $token = $oauth_client->getLongLivedAccessToken($facebookToken);
            $response = $fb->get('/me' . User::getFacebookFields(), $token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        $facebookData = $response->getGraphUser();
        $loggedUser = User::with('session')->where('facebook_id', $facebookData->getId())->first();

        $newSession = isset($loggedUser) ? $loggedUser->session : new Sessions();
        $loggedUser = isset($loggedUser) ? $loggedUser : new User();

        /* User */
        $loggedUser->name = $facebookData->getName();
        $loggedUser->facebook_id = $facebookData->getId();
        $loggedUser->email = $facebookData->getEmail();
        
        $picture = $facebookData->getPicture();
        $picture = json_decode($picture, TRUE);
        $loggedUser->picture = $picture['url'];
        
        /* Session */
        $uuid = Uuid::uuid4();
        $newSession->uuid = $uuid->toString();
        $newSession->facebook_token = (string) $token->getValue();
        $newSession->expiry_date = $token->getExpiresAt();

        DB::transaction(function () use($loggedUser, $newSession) {
            $loggedUser->save();
            $loggedUser->session()->save($newSession);
            $loggedUser->session = $newSession;
        }); 

        return $loggedUser;
    }
}
