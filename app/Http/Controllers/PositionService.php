<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FacebookFriends;
use App\Groups;
use App\Groups_elements;
use App\User;
use App\Sessions;
use App\Position_request_applicant;
use App\Position_requests;
use App\SendPushModel;
use Carbon\Carbon;
use SammyK;
use Illuminate\Support\Facades\Validator;


class PositionService extends Controller
{
    public function getUserLocationRequest($userFacebookId = NULL, SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, FacebookFriends $facebookFriends, SendPushModel $sendPush){
		$session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
		$requesterUser = $session->user;
		$requestedUser = User::with('session')->where('facebook_id', $userFacebookId)->first();

		if(!isset($requestedUser)){
            return response(['success' => FALSE,'message'=>'Invalid facebook_id ['. $userFacebookId .'] .'], 400);
		}

		/*if ($requestedUser->facebook_id == $userFacebookId) {
			return response(['success' => FALSE,'message'=>'You do not need to find yourself'], 400);
		}*/

		$userFriends = $facebookFriends->callFacebook($fb, $session);
		$isFriend = FALSE;

		//is Facebook friend?
		for ($i=0; $i < sizeof($userFriends); $i++) { 
			if($userFriends[$i]['facebook_id'] == $userFacebookId){
				$isFriend = TRUE;
			}
		}

		//If not, are they on the same group?
		if(!$isFriend){
			$requesterGroups = Groups_elements::where('user_id', $requesterUser->id)->all();
			for ($i=0; $i < sizeof($requesterGroups); $i++) { 
				$groupUsers = Groups_elements::where('group_id', $requesterGroups[$i]->group_id)->all();
				for ($j=0; $j < sizeof($groupUsers); $j++) { 
					if($groupUsers[$j]->user_id == $requestedUser->id){
						$isFriend = TRUE;
						break;
					}
				}
				if($isFriend){
					break;
				}
			}
		}
		

		if(!$isFriend){
            return response(['success' => FALSE, 'message'=>'Bad facebook_id request. You and ['. $userFacebookId .'] are not friends.'], 400);
		}

		//Get all the requested user position request where are the user has set the position
		$userPositionRequests = Position_requests::where('user_id', $requestedUser->id)->whereNotNull('latitude')->whereNotNull('longitude')->get();
		
		//If there is not any request ongoing 
		if(sizeof($userPositionRequests) > 0){

			foreach ($userPositionRequests as $userPositionRequest) {
				$actualDate = Carbon::now();
				$date = Carbon::parse($userPositionRequest->position_update_timestamp); 
				$diffDate = $date->diffInMinutes($actualDate);

				//Was the request done in the last 2 min?
				if($diffDate < 2){
					return response( ['success' => TRUE, "message" => "Success", 
							"position" => [ 
								"latitude" => $userPositionRequest->latitude,
								"longitude" => $userPositionRequest->longitude
							], 200]);
				}
			}
			$positionRequest = new Position_requests();
			//If there is not a request done in the last 2 min, create a new one	
			DB::transaction(function () use($positionRequest, $requestedUser, $requesterUser) {
				
			 	$positionRequest->user_id = $requestedUser->id;
			 	$positionRequest->request_timestamp = Carbon::now();
	            $positionRequest->save();

				$positionRequestApplicant = new Position_request_applicant();
	            $positionRequestApplicant->request_id = $positionRequest->id;
	            $positionRequestApplicant->user()->associate($requesterUser);
	            $positionRequestApplicant->save();
	        });

	        if ($sendPush->requestUserLocation($positionRequest->id, $userFacebookId)) {
	            return response(['success' => FALSE,'message'=>'New position request created to the user [' . $userFacebookId . ']'], 200);
	        } else {
	            return response(['success' => FALSE,'message'=>'Something went wrong sending the push notification.'], 400);
	        }
        }

        //Get the ongoing requests
		$userPositionRequest = Position_requests::where('user_id', $requestedUser->id)->where('latitude', NULL)->where('longitude', NULL)->first();
		
		//If the are ongoing requests
		if(isset($userPositionRequest)){

			//has the current user already done position request?
			if (Position_request_applicant::where('request_id', $userPositionRequest->id)->where('user_id', $requesterUser->id)->count()) {
				if ($sendPush->requestUserLocation($userPositionRequest->id, $userFacebookId)) {
		            return response(['success' => FALSE,'message'=>'New position request created to the user [' . $userFacebookId . ']'], 200);
		        } else {
		            return response(['success' => FALSE, 'message'=>'Something went wrong sending the push notification.'], 400);
		        }
				return response(['success' => FALSE, 'message'=>'Position request created to the user [' . $userFacebookId . ']'], 200);
			}



			$positionRequestApplicant = new Position_request_applicant();
			DB::transaction(function () use($requesterUser, $userPositionRequest) {
				$positionRequestApplicant = new Position_request_applicant();
	            $positionRequestApplicant->request_id = $userPositionRequest->id;
	            $positionRequestApplicant->user()->associate($requesterUser);
	            $positionRequestApplicant->save();
	        }); 


			return response(['success' => FALSE, 'message'=>'New position request created to the user [' . $userFacebookId . ']'], 200);

		}

		//IF there are no ongoing requests

		//If the user has no push token
		if($requestedUser->session->push_token == NULL){
			return response(['success' => FALSE,'message'=>'Could not execute the position request. The user [' . $userFacebookId . '] does not have a valid push token.'], 400);
		}

        $positionRequestApplicant = new Position_request_applicant();
        $positionRequest = new Position_requests();
		DB::transaction(function () use($positionRequest, $requestedUser, $requesterUser) {
		 	$positionRequest->user_id = $requestedUser->id;
		 	$positionRequest->request_timestamp = Carbon::now();
            $positionRequest->save();

			$positionRequestApplicant = new Position_request_applicant();
            $positionRequestApplicant->request_id = $positionRequest->id;
            $positionRequestApplicant->user()->associate($requesterUser);
            $positionRequestApplicant->save();
        }); 

        if ($sendPush->requestUserLocation($positionRequest->id, $userFacebookId)) {
            return response(['success' => FALSE,'message'=>'New position request created to the user [' . $userFacebookId . ']'], 200);
        } else {
            return response(['success' => FALSE, 'message'=>'Something went wrong sending the push notification.'], 400);
        }
	}

    public function askHelpToGroup($groupId, Request $request, SendPushModel $sendPush) {
        $session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
        if (!$session) {
            return response('The user doesnt exist.', 400);            
        }
		$user = $session->user;

		$group = Groups_elements::with('group')->where('user_id', $user->id)->where('group_id', $groupId)->first();
        if (!$group) {
            return response('The group dont exists or you dont belong there.', 400);            
        }

        $body = json_decode($request->getContent(), TRUE);
        if (isset($body['position']) && isset($body['position']['latitude']) && is_numeric($body['position']['latitude']) && isset($body['position']['longitude']) && is_numeric($body['position']['longitude'])) {
            $position = $body['position'];
        } else {
            return response('Bad structure. You should provide a valid structure.', 400);
        }

        $groupElements = Groups_elements::with('user')->where('group_id', $groupId)->get();
		if (!$groupElements) {
            return response('Group dont exits or its empty.', 400);
        }

		$usersId = [];
        for ($i=0; $i < sizeof($groupElements); $i++) {
            if ($user->id != $groupElements[$i]->user->id) {
			    $usersId[] = $groupElements[$i]->user->id;
            }
		}

        if ($sendPush->sendHelpToGroupElements($user, $usersId, $position)) {
            return "Success";
        } else {
            return response('Something went wrong sending the push notification.', 400);
        }
    }

    public function setPosition(Request $request)
    {

   		$validator = Validator::make($request->all(), [
            'request_id' => 'required|integer',
            'success' => 'required|boolean',
            'position' => 'required_if:success,==,true',
            'position.latitude' => 'required_if:success,==,true|numeric',
            'position.longitude' => 'required_if:success,==,true|numeric'
        ]);

        if ($validator->fails()) {
            return response('Invalid request strucutre.', 400);
        }

        $body = $request->all();
        
        $positionRequest = Position_requests::find($body['request_id']);

        if (!isset($positionRequest)) {
        	return response('Invalid request_id.', 400);
        }

        $session = Sessions::with('user')->where('uuid', Input::get('uuid'))->first();
		$user = $session->user;

        if ($positionRequest->user_id != $user->id) {
        	return response('Invalid access.', 400);
        }

        if (isset($positionRequest->position_update_timestamp)) {
        	return response('Position already defined.', 400);
        }

        $pushModel = new SendPushModel();

        $query_result = DB::table('users')
            ->join('position_request_applicants', 'position_request_applicants.user_id', '=', 'users.id')
            ->where('request_id', '=', $positionRequest->id)
            ->select('users.id')
            ->get();


		$users_ids = [];

		if (isset($query_result)) {
			foreach ($query_result as $element) {
				$users_ids[] = $element->id;
			}
		}

        if (!$body['success']) {
        	$pushModel->unableToSendUserLocation($user, $users_ids);
        	return "success";
        }

        $positionRequest->latitude = $body['position']['latitude'];
        $positionRequest->longitude = $body['position']['longitude'];
        $positionRequest->position_update_timestamp = Carbon::now();

        $positionRequest->save();

        $pushModel->sendUserLocation($user, $users_ids, $body['position']);

        return "success";
    }
}
