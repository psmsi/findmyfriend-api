<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Sessions;
use SammyK;

class FacebookFriends extends Controller
{
	public function getUsersFriends(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
		$loggedUser = Sessions::where('uuid', Input::get('uuid'))->first();

        if ($loggedUser == null){
        	return response()->json(['authExpire' => 'true']);
        }
        
        return response()->json($this->callFacebook($fb, $loggedUser));
	}

    public function callFacebook($fb, $loggedUser){
        try {
            $response = $fb->get('/me/friends'. User::getFacebookFields(), $loggedUser->facebook_token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        $friends = json_decode($response->getBody(), TRUE);

        if(!empty($friends['data'])) {
            for ($i=0; $i < sizeof($friends['data']); $i++ ) {
                $friends['data'][$i]['picture'] = $friends['data'][$i]['picture']['data']['url'];
                $friends['data'][$i]['facebook_id'] = $friends['data'][$i]['id'];
                unset($friends['data'][$i]['id']);
            }
        }
        return $friends['data'];
    }
}