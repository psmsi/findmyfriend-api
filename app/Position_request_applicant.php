<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position_request_applicant extends Model
{
    protected $table = 'position_request_applicants';

    public $incrementing = false;  
    
    public $timestamps = false;

    /**
     * Get the user that made the request.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the user that made the request.
     */
    public function positionRequest()
    {
        return $this->belongsTo('App\Position_requests');
    }
}
