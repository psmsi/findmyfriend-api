<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'facebook_id', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'remember_token', 'created_at', 'updated_at' 
    ];

    public static function getFacebookFields() {
        return "?fields=id,name,email,picture.type(large)";
    }

    /**
     * Get user's session.
     */
    public function session()
    {
        return $this->hasOne('App\Sessions');
    }

    /**
     * Get user's position requests.
     */
    public function positionRequests()
    {
        return $this->hasMany('App\Position_requests');
    }

    /**
     * Get user's position request applicant.
     */
    public function positionRequestApplicant()
    {
        return $this->hasMany('App\Position_request_applicant');
    }

    /**
     * Get the groups where the user belongs.
     */
    public function groups()
    {
        return $this->hasMany('App\Groups_elements');
    }
}
