<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups_elements extends Model
{
    protected $table = 'groups_elements';

    protected $fillable = ['user_id', 'group_id'];

    public $incrementing = false;  
    public $timestamps = false;

    /**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the group
     */
    public function group()
    {
        return $this->belongsTo('App\Groups');
    }
}
