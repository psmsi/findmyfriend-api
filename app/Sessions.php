<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    
    protected $table = 'sessions';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'facebook_token', 'expiry_date', 'push_token', 'uuid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'facebook_token', 'id', 'user_id', 'expiry_date', 'push_token', 'created_at', 'updated_at'
    ];

    /**
     * Get the user that owns the session.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
