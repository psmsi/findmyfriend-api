<?php

namespace App;

use Edujugon\PushNotification\PushNotification;
use App\User;


class SendPushModel 
{
    public function sendHelpToGroupElements(User $current_user, array $users_ids, $position) 
    {
        $notification['title'] = $current_user->name . " pediu para ser encontrado.";
        $notification['body'] = "Clique para obter a sua posição";

        return $this->_sendPushToUsers($current_user, $users_ids, $notification, $position);
    }

    public function sendUserLocation(User $current_user, array $users_ids, $position) 
    {
        $notification['title'] = $current_user->name . " partilhou a sua localização.";
        $notification['body'] = "Clique para ver no mapa.";

        return $this->_sendPushToUsers($current_user, $users_ids, $notification, $position);
    }

    public function unableToSendUserLocation(User $current_user, array $users_ids) 
    {
        $notification['title'] = $current_user->name . " não autorizou a partilha da sua localização.";
        $notification['body'] = "Tente novamente mais tarde.";

        return $this->_sendPushToUsers($current_user, $users_ids, $notification);
    }

    public function requestUserLocation($request_id, $user_facebook_id) 
    {
        $paylod['request_id'] = $request_id;
        $payload['action_key'] = 1;

        $user = User::with('session')->where('facebook_id', $user_facebook_id)->first();

        if (!isset($user) || !isset($user->session) || !isset($user->session->push_token)) {
            return FALSE;
        }

        return $this->_sendPush(NULL, $payload, [$user->session->push_token]);
    }

    private function _sendPushToUsers(User $current_user, array $users_ids, $notification, $position = NULL) {
        $pushTokens = [];
        foreach ($users_ids as $id) {
            $user = User::with('session')->find($id);

            if (isset($user) && isset($user->session) && isset($user->session->push_token)) {
                $pushTokens[] = $user->session->push_token;
            }
        }

        if (isset($position)) {
            $payload = $current_user->toArray();
            $payload['position'] = [
                'latitude' => $position['latitude'],
                'longitude' => $position['longitude']
            ];

            $payload['action_key'] = 2; //send position
        } else {
            $payload['action_key'] = 0; // Do nothing
        }
        

        return $this->_sendPush($notification, $payload, $pushTokens);
    }

    private function _sendPush($notification, $payload, $pushToken) 
    {
        $push = new PushNotification('fcm');

        $push->setMessage(['notification' => $notification, 'data' => $payload])
            ->setDevicesToken($pushToken)
            ->send();

        return TRUE;
    }
}
