<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = 'groups';

    public $timestamps = false;

    /**
     * Get the users that belongs to the group.
     */
    public function groupElements()
    {
        return $this->hasMany('App\Groups_elements');
    }
}
