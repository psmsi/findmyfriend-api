<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionRequestApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_request_applicants', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('request_id')->unsigned();
            $table->primary(['user_id', 'request_id']);
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->foreign('request_id')
                  ->references('id')
                  ->on('position_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_request_applicants');
    }
}
