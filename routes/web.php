<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('docs');
});

/* User isto para os serviços onde é preciso o user estar autenticado ver app/HTTP/MiddleWare/UserAuthenticated */
//->middleware('user-authenticated');

/* Facebook login*/
Route::get('/facebook/login/{facebookToken}', "FacebookAuth@getFacebookLoginURL");
Route::get('/facebook/friends', "FacebookFriends@getUsersFriends")->middleware('user-authenticated');

/* Push */
Route::get('/push/send/{pushToken}', "PushService@sendPush");
Route::post('/push/register', "PushService@registerToken")->middleware('user-authenticated');

/* Groups */
Route::post('/groups/new', "GroupsService@addGroup")->middleware('user-authenticated');
Route::get('/groups/list', "GroupsService@listGroups")->middleware('user-authenticated');
Route::delete('/groups/{groupId}/leave', "GroupsService@leaveGroup")->middleware('user-authenticated');
Route::post('/groups/{groupId}/add', "GroupsService@addElements")->middleware('user-authenticated');
Route::get('/groups/{groupId}/elements', "GroupsService@getElements")->middleware('user-authenticated');

/* Position */
Route::get('/positions/{facebookId}', "PositionService@getUserLocationRequest")->middleware('user-authenticated');
Route::post('/groups/{groupId}/help', "PositionService@askHelpToGroup")->middleware('user-authenticated');
Route::post('/position/set', "PositionService@setPosition")->middleware('user-authenticated');
